package com.trip.repository;

import com.trip.model.Deelnemer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface DeelnemerRepository extends JpaRepository<Deelnemer, Long> {

    Deelnemer findByEmail(String email);   //enabled by default
}
