package com.trip.repository;

import com.trip.model.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CategorieRepository extends JpaRepository<Categorie, Long> {
}
