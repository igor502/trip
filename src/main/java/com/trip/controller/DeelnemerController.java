package com.trip.controller;

import com.trip.model.Deelnemer;
import com.trip.repository.DeelnemerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class DeelnemerController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DeelnemerRepository deelnemerRepository;

    @RequestMapping(method = RequestMethod.POST)
    public String createUser(@RequestHeader("email") String email, @RequestHeader("password") String password) {
        Deelnemer deelnemer = null;
        try {
            deelnemer = new Deelnemer(email, password);
            deelnemerRepository.save(deelnemer);
        } catch (Exception ex) {
            return "Error while creating customer with email " + email + "due to: " + ex.toString();
        }
        return "Deelnemer succesfully created! (id = " + deelnemer.getId() + ")";
    }

    @RequestMapping(method = RequestMethod.GET)
    public Deelnemer getUser(@RequestHeader("email") String email, @RequestHeader("password") String password){
        try{
            Deelnemer deelnemer = deelnemerRepository.findByEmail(email);
            if(password.equals(deelnemer.getPassword())){
                return deelnemer;
            } else {
                throw new Exception("password or username is wrong");
            }
        } catch (Exception ex) {
            logger.error(ex.toString());
            return null;
        }
    }
}
