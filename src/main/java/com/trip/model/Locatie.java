package com.trip.model;

import com.trip.constant.LocatieType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name="locatie")
public class Locatie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ManyToMany(mappedBy = "locaties")
    private List<Trip> trips;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "locatie")
    private List<TripLocatie> tripLocaties;


    @Column
    private String foto;


}
