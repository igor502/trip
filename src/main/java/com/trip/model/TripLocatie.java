package com.trip.model;

import com.trip.constant.LocatieType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Igor on 2/07/2017.
 */
public class TripLocatie {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;


    @NotNull
    @ManyToOne
    @JoinColumn(name = "id")
    private Locatie locatie;


    @NotNull
    @Column
    private LocatieType locatieType;

    @NotNull
    @Column
    private int volgordenr;


    @NotNull
    @Column
    private String vrijeTekst;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id")
    private Trip trip;








}
