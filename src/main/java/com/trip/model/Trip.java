package com.trip.model;

import com.sun.istack.internal.Nullable;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name="trip")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "naam")
    private String naam;

    @Nullable
    @Column(name = "omschrijving")
    private String omschrijving;



    @ManyToMany
    @JoinTable(name ="trip_categorie", joinColumns = @JoinColumn(name = "trip_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "categorie_id", referencedColumnName = "id"))
    private List<Categorie> categories;

    @ManyToMany
    @JoinTable(name="trip_deelnemer", joinColumns = @JoinColumn(name = "trip_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "deelnemer_id", referencedColumnName = "id"))
    private List<Deelnemer> deelnemers;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trip")
    private List<TripLocatie> tripLocaties;
}
